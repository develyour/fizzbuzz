let expect  = require("chai").expect;
let request = require("request");
let Step1 = require('../Step1/Step1');
let Step2 = require('../Step2/Step2');
let Step3 = require('../Step3/Step3');
const readline = require('readline-sync');

describe("Our application", function() {

    it("returns status result on Step1", function(done) {

        let result = Step1(1, 20);
        if(result == true){
            done()
        }
        else{
            done(new Error("Not sure what's happened ON STEP1."));
        }

    });

    it("returns status result on Step2", function(done) {
        
        let result = Step2(1, 20)
        if(result == true){
            done()
        }
        else{
            done(new Error("Not sure what's happened ON STEP2."));
        }

    });


    it("returns status result on Step3", function(done) {
        
        let result = Step3()
        if(result == true){
            this.timeout(10000);
            done()
        }
        else{
            done(new Error("Not sure what's happened ON STEP3."));
        }

    });

});