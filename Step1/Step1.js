const start = process.argv[2]
const stop = process.argv[3]

function Step1(start, stop){
    
    for (let i = start; i <= stop; i++)
    {
        if (i % 15 == 0)
            console.log("FizzBuzz");
        else if (i % 3 == 0)
            console.log("Fizz");
        else if (i % 5 == 0)
            console.log("Buzz");
        else
            console.log(i);
    }
}

Step1(start, stop)

module.exports = function (start, stop ){
    for (let i = start; i <= stop; i++)
    {
        if (i % 15 == 0)
            console.log("FizzBuzz");
        else if (i % 3 == 0)
            console.log("Fizz");
        else if (i % 5 == 0)
            console.log("Buzz");
        else
            console.log(i);
    }

    if(start !== '' && stop !== ''){
        return true
    }
    else{
        return false
    }
}
