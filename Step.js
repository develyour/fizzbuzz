const start = process.argv[2]
const stop = process.argv[3]

function Step1(){
    console.log("Step1: \n")
    for (let i = start; i <= stop; i++)
    {
        if (i % 15 == 0)
            console.log("FizzBuzz");
        else if (i % 3 == 0)
            console.log("Fizz");
        else if (i % 5 == 0)
            console.log("Buzz");
        else
            console.log(i);
    }
}

Step1()

function Step2(){
    console.log("\n\nStep2 \n")
    for (let i = start; i <= stop; i++)
    {
        if (i.toString().includes(3))
            console.log("lucky");
        else if( i % 15 == 0)
            console.log("FizzBuzz");
        else if (i % 3 == 0)
            console.log("fizz");
        else if (i % 5 == 0)
            console.log("Buzz");
        else
            console.log(i);
    }
}

Step2()

let fizzCounter = 0;
let buzzCounter = 0;
let fizzbuzzCounter = 0;
let luckyCounter = 0;
let integerCounter = 0;

function Step3(){
    console.log("\n\nStep3\n")
    for (let i = start; i <= stop; i++)
    {
        if (i.toString().includes(3)){
            console.log("lucky");
            luckyCounter++;
        }
        else if (i % 15 == 0){
            console.log("FizzBuzzizz");
            fizzbuzzCounter++;
        }
        else if (i % 5 == 0){
            console.log("Buzz");
            buzzCounter++;
        }
        else if( i % 3 == 0){
            console.log("fizz");
            fizzCounter++;
        }
        else{
            console.log(i);
            integerCounter++;
        }
    }

    console.log(  "\nfizz = " + fizzCounter + "; buzz = " + buzzCounter + "; fizzbuzz = " + fizzbuzzCounter + "; lucky = " + luckyCounter + "; integer  = " + integerCounter);

}

Step3()
